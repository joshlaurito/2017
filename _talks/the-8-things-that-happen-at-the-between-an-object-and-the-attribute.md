---
abstract: We rarely think about the dot "." between our objects and their fields,
  but there are quite a lot of things that happen every time we use one in Python.  This
  talk will include the commonly known functions, (__dict__, __getattr__, etc), but
  especially focus on the Descriptor Protocol.
duration: 25
level: Intermediate
room: PennTop South
slot: 2017-10-07 13:30:00-04:00
speakers:
- Andy Fundinger
title: The 8 things that happen at the "." between an object and the attribute
type: talk
---

We rarely think about the dot "." between our objects and their fields, but there are quite a lot of things that happen every time we use one in Python. This talk will explore the details of what happens, how the descriptor protocol works, and how it can be used to alter the Python object model.

Actions to be explored:
1.  The instance __dict__ is checked.
2.  The class __dict__ is checked.
3.  The base classes __dicts__ are checked.
4.  __getattr__ runs.
5.  __getattribute__ runs.
6.  The __get__ method on the object in the class __dict__ is called.
7.  The object in the class __dict__ is checked for a __set__ method.
8.  raise AttributeError

Most of the talk will focus on 6 & 7 to explain the Descriptor protocol. Examples will include altering method binding and property behaviors, as well as supporting method implementation reuse via Descriptors.
