---
title: Frequently Asked Questions
---

## I'm speaking at the conference. Will you cover my travel and lodging costs?
{{ site.data.event.name }} is happy to offer financial aid for people traveling to
the conference. Follow [@PyGotham](https://twitter.com/PyGotham) or check back
here for updates.

## I want to volunteer.
Great! We’re always looking for people to be session chairs, to help with
registration, and to be general runners. Please sign up for one of the available
openings in the [volunteer signup
form](https://docs.google.com/spreadsheets/d/1emLgIN4o1m_4vHmfux2h9A95qrOPcgtZYuMPu0lNxyM/edit#gid=1196793555),
and we'll reach out with more details as we get closer to the event. Feel free
to ping us at [volunteers@pygotham.org](mailto:volunteers@pygotham.org) if you
have any questions.

## Do you offer financial assistance?
Absolutely! We aim for {{ site.data.event.name }} to be open to all to attend,
and we strive to make the conference as accessible as possible. Details of our
financial aid can be found on the [Registration Information]({% link
registration/index.md %}) page.

## I can only attend part of the conference. Are partial tickets available?
Unfortunately we don't have a way to limit passes to only part of the
conference, so only full access tickets are available.

{% if site.data.event.repository_url %}
## Your website is great, but I found a typo or otherwise missing information.
Thanks for pointing that out! Please submit a fix at
[{{ site.data.event.repository_url }}]({{ site.data.event.repository_url }}).
{% endif %}

## I have another question, and I solemnly swear that it is not answered on this page already.
We're happy to answer it for you. Email us at
[organizers@pygotham.org](mailto:organizers@pygotham.org).
