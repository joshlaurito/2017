---
title: "Talk Selection Process Retrospective"
date: 2017-09-25 12:30:00 -0400
---

Dan Crosta, one of the PyGotham 2017 Program Committee Chairs has shared
some thoughts on the talk selection process this year, and how we're aiming
to improve it for next year. [Read more on Dan's
blog.](https://late.am/post/2017/09/25/pygotham-talk-voting-retrospective.html)
