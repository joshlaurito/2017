---
name: Jordan Yaker
talks:
- Try-Monads With Big Data Using PySpark
---

Jordan Yaker is the CEO and Founder of hireross.com. After years of working as a consultant he has joined the product side of the industry and is working on building the next generation of accounting automation tools. When he's not dabbling with code, he can often be found trying to coral his family and work with the two tiny humans that he and his wife have deployed to <del>production</del> reality.