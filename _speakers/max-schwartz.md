---
name: Max Schwartz
talks:
- "Building a \u201CPun Generator\u201D in Python"
---

In 2016 I earned my Masters in Linguistics with a concentration in Computational Linguistics. Since April I have been working as a Research Engineer on the Natural Language Processing team at the Educational Testing Service. I am also a tournament chess player, a musician, and have been known to evoke pained groans from friends with some especially bad puns.