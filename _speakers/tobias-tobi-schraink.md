---
name: Tobias "Tobi" Schraink
talks:
- Why you (!) should teach kids how to code
---

I am a PhD student in Itai Yanai’s lab at the Institute for Computational Medicine for NYU School of Medicine. Though my background is in molecular biology, the wonderful python ecosystem and its inhabitants have made it possible for me to answer computational questions myself. Currently I am analyzing the genomes of one charming bacterium called M. tuberculosis.