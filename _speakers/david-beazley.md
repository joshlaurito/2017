---
name: David Beazley
talks:
- The Other Async
---

David Beazley is the author of the Python Cookbook (O'Reilly Media) and the
Python Essential Reference (Addison-Wesley). He's been involved with Python
since 1996.
