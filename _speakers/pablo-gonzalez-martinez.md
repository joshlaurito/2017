---
name: "Pablo Gonz\xE1lez Mart\xEDnez"
talks:
- Darth Linguo, building an ungrammatical corpus by corruption
---

I am in the 4th year of my Ph.D in Linguistics at the CUNY Graduate Center focusing in Computational Linguistics. My background is quite varied, I have dual bachelors in Literature and Mathematics and worked as a translator and interpreter back in my native Colombia. I always find myself in intermediate fields and one of my ambitions is to fill in the gap between computer scientist and linguists in NLP and computational linguistics, driving the field towards more integrated approaches like the one I would like to present here.