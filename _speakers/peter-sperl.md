---
name: Peter Sperl
talks:
- High Performance Microservices
---

I'm an Engineering Manager at Bloomberg LP where I've worked since 2008.  I graduated from Carnegie Mellon University in 2004 with a Bachelor's in Electrical and Computer Engineering.  I've been a part of two startups, both of which were funded, and put in some time at Northrop Grumman before taking my current position at Bloomberg.