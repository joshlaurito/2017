---
name: iHeartMedia
tier: gold
site_url: https://iheartmedia.com/
logo: iheart.png
---
With over a quarter of a billion monthly listeners in the U.S. and over 85 million social followers,
iHeartMedia has the largest reach of any radio or television outlet in America. It serves over 150
markets through 858 owned radio stations, and the company’s radio stations and content can be heard
on AM/FM, HD digital radio, satellite radio, on the Internet at iHeartRadio.com and on the company’s
radio station websites, on the iHeartRadio mobile app, in enhanced auto dashes, on tablets and
smartphones, and on gaming consoles.

iHeartRadio, iHeartMedia’s digital radio platform, is the No. 1 all-in-one digital audio service
with over a billion downloads; it reached its first 20 million registered users faster than any
digital service in Internet history and reached 90 million registered users faster than any other
radio or digital music service and even faster than Facebook. The company’s operations include radio
broadcasting, online, mobile, digital and social media, live concerts and events, syndication, music
research services and independent media representation.
